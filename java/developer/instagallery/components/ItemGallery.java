package developer.instagallery.components;

/**
 * Created by Filip Krasniqi on 28/03/2017.
 * Classe che contiene le informazioni per il singolo elemento di galleria.
 */

public class ItemGallery {
    private String fileName;

    public ItemGallery(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
