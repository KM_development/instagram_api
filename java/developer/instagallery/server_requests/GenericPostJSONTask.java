package developer.instagallery.server_requests;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by fkras on 28/03/2016.
 */
/*
* Classe astratta che consente la comunicazione asincrona con protocollo HTTP con metodo POST, tramite libreria OkHttp, ritornando un JSON.
* E' necessaria la reimplementazione della funzione onPostExecute ogni volta che si utilizza questa classe.
* */
public abstract class GenericPostJSONTask extends AsyncTask<PostComponents, Void, Object> {
    private Object post(PostComponents pc) throws IOException {
        OkHttpClient client = new OkHttpClient();
        String getUrl = pc.getUrl();
        FormBody.Builder fe = new FormBody.Builder();

        if(pc.getJsonArrString() != null) {
            for(int i=0;i<pc.getJsonArrString().size();i++) {
                fe.add(pc.getJsonArrString().get(i).first,pc.getJsonArrString().get(i).second);
            }
        }

        Request request = null;
        request = new Request.Builder()
                .url(getUrl)
                .post(fe.build())
                .build();

        Response response = null;
        response = client.newCall(request).execute();
        String resp = "";
        try {
            resp = response.body().string();
            JSONObject tempJsOb = new JSONObject(resp);
            tempJsOb.accumulate("httpResultCode", response.code());
            //Log.e("JSON RESP", tempJsOb.toString());
            return tempJsOb;
        } catch (JSONException e) {
            try {
                JSONArray tempJsArr = new JSONArray(resp);
                return tempJsArr;
            } catch(JSONException e2) {
                JSONObject tempJsOb = new JSONObject();
                try {
                    tempJsOb.accumulate("httpResultCode", response.code());
                } catch (JSONException e1) {

                }
                return tempJsOb;
            }
        } catch (IOException e) {
            Log.e("ERRORE", "2");
            //return null;
        }
        return  null;
    }

    @Override
    protected Object doInBackground(PostComponents... pcs) {
        try {
            Object ret = post(pcs[0]);
            return ret;
        } catch (IOException e) {
            return null;
        }
    }


    // lascio vuota: implementare come si vuole facendo extends di GenericGetJSONArrayTask con <Specific>PostTask
    protected abstract void onPostExecute(final Object jsonResponse);
}
