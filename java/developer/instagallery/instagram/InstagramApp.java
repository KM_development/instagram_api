package developer.instagallery.instagram;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import developer.instagallery.R;
import developer.instagallery.server_requests.GenericPostJSONTask;
import developer.instagallery.server_requests.PostComponents;

/**
 * 
 * @author Thiago Locatelli <thiago.locatelli@gmail.com>
 * @author Lorensius W. L T <lorenz@londatiga.net>
 * 
 */
public class InstagramApp {
	
	private InstagramSession mSession;
	private ProgressDialog mProgress;
	private String mAuthUrl;
	private String mAccessToken;
	private Context mCtx;
	
	private String mClientId;
	private String mClientSecret;

	public static String mCallbackUrl = "";
	private static final String AUTH_URL = "https://api.instagram.com/oauth/authorize/";
	private static final String TOKEN_URL = "https://api.instagram.com/oauth/access_token";
	private static final String API_URL = "https://api.instagram.com/v1/";
    public static final String USER_API_URL = API_URL+"users/";
    public static final String MEDIA_API_URL = "/media/recent";

	private static final String TAG = "InstagramAPI";

	public InstagramApp(Context context, String clientId, String clientSecret,
			String callbackUrl, ProgressDialog mProgress) {
		
		mClientId = clientId;
		mClientSecret = clientSecret;
		mCtx = context;
		this.mProgress = mProgress;
		mSession = new InstagramSession(context);
		mAccessToken = mSession.getAccessToken();
		mCallbackUrl = callbackUrl;
		mAuthUrl = AUTH_URL + "?client_id=" + clientId + "&redirect_uri="
				+ mCallbackUrl + "&response_type=code&display=touch";
	}

	public String getmAuthUrl() {
		return mAuthUrl;
	}

	//funzione che in modo asincrono scarica il token dato il codice: passaggio all'activity avviene tramite broadcastReceiver
	public void getAccessToken(final String code) {
		mProgress.setTitle("Getting access token ...");
		//mProgress.show();

        GenericPostJSONTask getAccessTokenTask = new GenericPostJSONTask() {
            @Override
            protected void onPostExecute(Object obj) {
                JSONObject jsonObj = (JSONObject)obj;

                //JSONObject jsonObj = (JSONObject) new JSONTokener(response).nextValue();
                try {
                    mAccessToken = jsonObj.getString("access_token");
                    Log.i(TAG, "Got access token: " + mAccessToken);

                    String id = jsonObj.getJSONObject("user").getString("id");
                    String user = jsonObj.getJSONObject("user").getString("username");
                    String name = jsonObj.getJSONObject("user").getString("full_name");

                    mSession.storeAccessToken(mAccessToken, id, user, name);
					mProgress.dismiss();

					Intent intent = new Intent();
					intent.setAction(mCtx.getString(R.string.token_received));
					mCtx.sendBroadcast(intent);
                    //mDialog.dismiss();
                } catch (NullPointerException | JSONException ex) {
                    Log.e("", "");
                    //realm.cancelTransaction();
                    //errorDownloading = true;
                }
            }
        };
        ArrayList<Pair<String, String>> jsonArray = new ArrayList<>();
        jsonArray.add(new Pair<String, String>("client_id", mClientId));
        jsonArray.add(new Pair<String, String>("client_secret", mClientSecret));
        jsonArray.add(new Pair<String, String>("grant_type", "authorization_code"));
        jsonArray.add(new Pair<String, String>("redirect_uri", mCallbackUrl));
        jsonArray.add(new Pair<String, String>("code", code));
        PostComponents pc = new PostComponents(TOKEN_URL, jsonArray);
        getAccessTokenTask.execute(pc);
	}

	public boolean hasAccessToken() {
		return (mAccessToken == null) ? false : true;
	}

	public String getId() {
		return mSession.getId();
	}
	
	public String getName() {
		return mSession.getName();
	}

	//funzione che consente il logout
	public void resetAccessToken() {
		if (mAccessToken != null) {
			mSession.resetAccessToken();
			mAccessToken = null;
		}
	}

    public String getmAccessToken() {
        return mAccessToken;
    }
}